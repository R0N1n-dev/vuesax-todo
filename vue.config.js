module.exports = {
  configureWebpack: {
    plugins: [require("unplugin-vue-components/webpack")({})],
  },
  pluginOptions: {
    windicss: {},
  },
  // ...other vue-cli plugin options...
  pwa: {
    workboxPluginMode: "InjectManifest",
    workboxOptions: {
      // swSrc is required in InjectManifest mode.
      swSrc: "src/sw.js",
      swDest: "service-worker.js",
    },
  },
};
