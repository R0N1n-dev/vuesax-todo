import Vue from "vue";
import Vuesax from "vuesax";
import "vuesax/dist/vuesax.css";
import "windi.css";
import "material-icons/iconfont/material-icons.css";
import App from "./App.vue";
import "./registerServiceWorker";

Vue.config.productionTip = false;
Vue.use(Vuesax);
new Vue({
  render: (h) => h(App),
}).$mount("#app");
